<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/bootstrap_barrio/subtheme/templates/pages/page--starter-kit.html.twig */
class __TwigTemplate_b7048ecaa63fb2d7d03366c14d223980137ae9286b465a9ec8db393a0b74a263 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'featured' => [$this, 'block_featured'],
            'content' => [$this, 'block_content'],
            'footer' => [$this, 'block_footer'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["block" => 73, "if" => 116];
        $filters = ["t" => 72, "escape" => 118];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['block', 'if'],
                ['t', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453 = $this->env->getExtension("Drupal\\webprofiler\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->enter($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "themes/bootstrap_barrio/subtheme/templates/pages/page--starter-kit.html.twig"));

        // line 70
        echo "<div id=\"page-wrapper\">
  <div id=\"page\">
    <header id=\"header\" class=\"header\" role=\"banner\" aria-label=\"";
        // line 72
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Site header"));
        echo "\">
      ";
        // line 73
        $this->displayBlock('head', $context, $blocks);
        // line 115
        echo "    </header>
    ";
        // line 116
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 117
            echo "      <div class=\"highlighted\">
        <aside class=\"";
            // line 118
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
            echo " section clearfix\" role=\"complementary\">
          ";
            // line 119
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
            echo "
        </aside>
      </div>
    ";
        }
        // line 123
        echo "    ";
        if ($this->getAttribute(($context["page"] ?? null), "featured_top", [])) {
            // line 124
            echo "      ";
            $this->displayBlock('featured', $context, $blocks);
            // line 131
            echo "    ";
        }
        // line 132
        echo "    <div id=\"main-wrapper\" class=\"layout-main-wrapper clearfix\">
      ";
        // line 133
        $this->displayBlock('content', $context, $blocks);
        // line 255
        echo "    </div>
    ";
        // line 256
        if ((($this->getAttribute(($context["page"] ?? null), "featured_bottom_first", []) || $this->getAttribute(($context["page"] ?? null), "featured_bottom_second", [])) || $this->getAttribute(($context["page"] ?? null), "featured_bottom_third", []))) {
            // line 257
            echo "      <div class=\"featured-bottom\">
        <aside class=\"";
            // line 258
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
            echo " clearfix\" role=\"complementary\">
          ";
            // line 259
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "featured_bottom_first", [])), "html", null, true);
            echo "
          ";
            // line 260
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "featured_bottom_second", [])), "html", null, true);
            echo "
          ";
            // line 261
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "featured_bottom_third", [])), "html", null, true);
            echo "
        </aside>
      </div>
    ";
        }
        // line 265
        echo "    <footer class=\"site-footer\">
      ";
        // line 266
        $this->displayBlock('footer', $context, $blocks);
        // line 283
        echo "    </footer>
  </div>
</div>
";
        
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->leave($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof);

    }

    // line 73
    public function block_head($context, array $blocks = [])
    {
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453 = $this->env->getExtension("Drupal\\webprofiler\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->enter($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "head"));

        // line 74
        echo "        ";
        if ((($this->getAttribute(($context["page"] ?? null), "secondary_menu", []) || $this->getAttribute(($context["page"] ?? null), "top_header", [])) || $this->getAttribute(($context["page"] ?? null), "top_header_form", []))) {
            // line 75
            echo "          <nav";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["navbar_top_attributes"] ?? null)), "html", null, true);
            echo ">
          ";
            // line 76
            if (($context["container_navbar"] ?? null)) {
                // line 77
                echo "          <div class=\"container\">
          ";
            }
            // line 79
            echo "              ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "secondary_menu", [])), "html", null, true);
            echo "
              ";
            // line 80
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "top_header", [])), "html", null, true);
            echo "
              ";
            // line 81
            if ($this->getAttribute(($context["page"] ?? null), "top_header_form", [])) {
                // line 82
                echo "                <div class=\"form-inline navbar-form float-right\">
                  ";
                // line 83
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "top_header_form", [])), "html", null, true);
                echo "
                </div>
              ";
            }
            // line 86
            echo "          ";
            if (($context["container_navbar"] ?? null)) {
                // line 87
                echo "          </div>
          ";
            }
            // line 89
            echo "          </nav>
        ";
        }
        // line 91
        echo "        <nav";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["navbar_attributes"] ?? null)), "html", null, true);
        echo ">
          ";
        // line 92
        if (($context["container_navbar"] ?? null)) {
            // line 93
            echo "          <div class=\"container\">
          ";
        }
        // line 95
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
        echo "
            ";
        // line 96
        if (($this->getAttribute(($context["page"] ?? null), "primary_menu", []) || $this->getAttribute(($context["page"] ?? null), "header_form", []))) {
            // line 97
            echo "              <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#CollapsingNavbar\" aria-controls=\"CollapsingNavbar\" aria-expanded=\"false\" aria-label=\"Toggle navigation\"><span class=\"navbar-toggler-icon\"></span></button>
              <div class=\"collapse navbar-collapse\" id=\"CollapsingNavbar\">
                ";
            // line 99
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "primary_menu", [])), "html", null, true);
            echo "
                ";
            // line 100
            if ($this->getAttribute(($context["page"] ?? null), "header_form", [])) {
                // line 101
                echo "                  <div class=\"form-inline navbar-form float-right\">
                    ";
                // line 102
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header_form", [])), "html", null, true);
                echo "
                  </div>
                ";
            }
            // line 105
            echo "\t          </div>
            ";
        }
        // line 107
        echo "            ";
        if (($context["sidebar_collapse"] ?? null)) {
            // line 108
            echo "              <button class=\"navbar-toggler navbar-toggler-left\" type=\"button\" data-toggle=\"collapse\" data-target=\"#CollapsingLeft\" aria-controls=\"CollapsingLeft\" aria-expanded=\"false\" aria-label=\"Toggle navigation\"></button>
            ";
        }
        // line 110
        echo "          ";
        if (($context["container_navbar"] ?? null)) {
            // line 111
            echo "          </div>
          ";
        }
        // line 113
        echo "        </nav>
      ";
        
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->leave($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof);

    }

    // line 124
    public function block_featured($context, array $blocks = [])
    {
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453 = $this->env->getExtension("Drupal\\webprofiler\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->enter($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "featured"));

        // line 125
        echo "        <div class=\"featured-top\">
          <aside class=\"featured-top__inner section ";
        // line 126
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo " clearfix\" role=\"complementary\">
            ";
        // line 127
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "featured_top", [])), "html", null, true);
        echo "
          </aside>
        </div>
      ";
        
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->leave($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof);

    }

    // line 133
    public function block_content($context, array $blocks = [])
    {
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453 = $this->env->getExtension("Drupal\\webprofiler\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->enter($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 134
        echo "        <div id=\"main\" class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["row"] ?? null)), "html", null, true);
        echo "\">










          <!-- partial:index.partial.html -->
          <div class=\"skw-pages\">
            <div class=\"skw-page skw-page-1 active\">
              <div class=\"skw-page__half skw-page__half--left\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\"></div>
                </div>
              </div>
              <div class=\"skw-page__half skw-page__half--right\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\">
                    <h2 class=\"skw-page__heading\">Starter kit</h2>
                    <p class=\"skw-page__description\">
                The starter kit comprises of all what is required for production of the black soldier fly larvae. The items are as follows;
                    <ol>
                      <li>Love cage</li>
                      <li>Feeding troughs</li>
                      <li>Larvae</li>
                    </ol>
                </p>
                  </div>
                </div>
              </div>
            </div>
            <div class=\"skw-page skw-page-2\">
              <div class=\"skw-page__half skw-page__half--left\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\">
                   <!-- <h2 class=\"skw-page__heading\">Page 2</h2>-->
                    <p class=\"skw-page__description\">Once the farmer receives the full package of the starter kit they are expected to feed the larvae until they turn black. <br/>
          Once the larvae turn black the farmer needs to remove some of the waste leaving them with just some waste to give them a place where they will hide and grow further to become the fly. </p>
                  </div>
                </div>
              </div>
              <div class=\"skw-page__half skw-page__half--right\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\"></div>
                </div>
              </div>
            </div>
            <div class=\"skw-page skw-page-3\">
              <div class=\"skw-page__half skw-page__half--left\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\"></div>
                </div>
              </div>
              <div class=\"skw-page__half skw-page__half--right\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\">
               <!--     <h2 class=\"skw-page__heading\">Page 3</h2> -->
                    <p class=\"skw-page__description\">During this time the farmer should cover the container with a net to ensure that once any fly emerges it remains there.<br/>
          Once you start having flies you need to move the container to the love cage where they will continue to pupate turning into flies.
          </p>
                  </div>
                </div>
              </div>
            </div>
            <div class=\"skw-page skw-page-4\">
              <div class=\"skw-page__half skw-page__half--left\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\">
              <!--      <h2 class=\"skw-page__heading\">Page 4</h2> -->
                    <p class=\"skw-page__description\">After every three days you need to collect the carton pieces and place on top of a trough with some food waste give them four days and they will have hatched.
                <br/>
                Once they are 5 days old start feeding them with each trough doing 1.5 kgs of waste and on the fourth day from when you feed them. <br/> Feed them more with 2.5 kgs of waste and on the eighth day feed them with 4kgs.<br/> From that point give them more time to feed and mature.

                </p>
                  </div>
                </div>
              </div>
              <div class=\"skw-page__half skw-page__half--right\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\"></div>
                </div>
              </div>
            </div>
            <div class=\"skw-page skw-page-5\">
              <div class=\"skw-page__half skw-page__half--left\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\"></div>
                </div>
              </div>
              <div class=\"skw-page__half skw-page__half--right\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\">
                   <!-- <h2 class=\"skw-page__heading\">Epic finale</h2> -->
                    <p class=\"skw-page__description\">
                      From that point you can feed them to the chicken, pig or fish with the waste remaining feeding it to your plants as organic fertilizer.

                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- partial -->

          <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>










        </div>
      ";
        
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->leave($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof);

    }

    // line 266
    public function block_footer($context, array $blocks = [])
    {
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453 = $this->env->getExtension("Drupal\\webprofiler\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->enter($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "footer"));

        // line 267
        echo "        <div class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo "\">
          ";
        // line 268
        if (((($this->getAttribute(($context["page"] ?? null), "footer_first", []) || $this->getAttribute(($context["page"] ?? null), "footer_second", [])) || $this->getAttribute(($context["page"] ?? null), "footer_third", [])) || $this->getAttribute(($context["page"] ?? null), "footer_fourth", []))) {
            // line 269
            echo "            <div class=\"site-footer__top clearfix\">
              ";
            // line 270
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_first", [])), "html", null, true);
            echo "
              ";
            // line 271
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_second", [])), "html", null, true);
            echo "
              ";
            // line 272
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_third", [])), "html", null, true);
            echo "
              ";
            // line 273
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_fourth", [])), "html", null, true);
            echo "
            </div>
          ";
        }
        // line 276
        echo "          ";
        if ($this->getAttribute(($context["page"] ?? null), "footer_fifth", [])) {
            // line 277
            echo "            <div class=\"site-footer__bottom\">
              ";
            // line 278
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_fifth", [])), "html", null, true);
            echo "
            </div>
          ";
        }
        // line 281
        echo "        </div>
      ";
        
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->leave($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof);

    }

    public function getTemplateName()
    {
        return "themes/bootstrap_barrio/subtheme/templates/pages/page--starter-kit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  466 => 281,  460 => 278,  457 => 277,  454 => 276,  448 => 273,  444 => 272,  440 => 271,  436 => 270,  433 => 269,  431 => 268,  426 => 267,  420 => 266,  291 => 134,  285 => 133,  274 => 127,  270 => 126,  267 => 125,  261 => 124,  253 => 113,  249 => 111,  246 => 110,  242 => 108,  239 => 107,  235 => 105,  229 => 102,  226 => 101,  224 => 100,  220 => 99,  216 => 97,  214 => 96,  209 => 95,  205 => 93,  203 => 92,  198 => 91,  194 => 89,  190 => 87,  187 => 86,  181 => 83,  178 => 82,  176 => 81,  172 => 80,  167 => 79,  163 => 77,  161 => 76,  156 => 75,  153 => 74,  147 => 73,  137 => 283,  135 => 266,  132 => 265,  125 => 261,  121 => 260,  117 => 259,  113 => 258,  110 => 257,  108 => 256,  105 => 255,  103 => 133,  100 => 132,  97 => 131,  94 => 124,  91 => 123,  84 => 119,  80 => 118,  77 => 117,  75 => 116,  72 => 115,  70 => 73,  66 => 72,  62 => 70,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Bootstrap Barrio's theme implementation to display a single page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.html.twig template normally located in the
 * core/modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - base_path: The base URL path of the Drupal installation. Will usually be
 *   \"/\" unless you have installed Drupal in a sub-directory.
 * - is_front: A flag indicating if the current page is the front page.
 * - logged_in: A flag indicating if the user is registered and signed in.
 * - is_admin: A flag indicating if the user has permission to access
 *   administration pages.
 *
 * Site identity:
 * - front_page: The URL of the front page. Use this instead of base_path when
 *   linking to the front page. This includes the language domain or prefix.
 * - logo: The url of the logo image, as defined in theme settings.
 * - site_name: The name of the site. This is empty when displaying the site
 *   name has been disabled in the theme settings.
 * - site_slogan: The slogan of the site. This is empty when displaying the site
 *   slogan has been disabled in theme settings.

 * Page content (in order of occurrence in the default page.html.twig):
 * - node: Fully loaded node, if there is an automatically-loaded node
 *   associated with the page and the node ID is the second argument in the
 *   page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - page.top_header: Items for the top header region.
 * - page.top_header_form: Items for the top header form region.
 * - page.header: Items for the header region.
 * - page.header_form: Items for the header form region.
 * - page.highlighted: Items for the highlighted region.
 * - page.primary_menu: Items for the primary menu region.
 * - page.secondary_menu: Items for the secondary menu region.
 * - page.featured_top: Items for the featured top region.
 * - page.content: The main content of the current page.
 * - page.sidebar_first: Items for the first sidebar.
 * - page.sidebar_second: Items for the second sidebar.
 * - page.featured_bottom_first: Items for the first featured bottom region.
 * - page.featured_bottom_second: Items for the second featured bottom region.
 * - page.featured_bottom_third: Items for the third featured bottom region.
 * - page.footer_first: Items for the first footer column.
 * - page.footer_second: Items for the second footer column.
 * - page.footer_third: Items for the third footer column.
 * - page.footer_fourth: Items for the fourth footer column.
 * - page.footer_fifth: Items for the fifth footer column.
 * - page.breadcrumb: Items for the breadcrumb region.
 *
 * Theme variables:
 * - navbar_top_attributes: Items for the header region.
 * - navbar_attributes: Items for the header region.
 * - content_attributes: Items for the header region.
 * - sidebar_first_attributes: Items for the highlighted region.
 * - sidebar_second_attributes: Items for the primary menu region.
 * - sidebar_collapse: If the sidebar_first will collapse.
 *
 * @see template_preprocess_page()
 * @see bootstrap_barrio_preprocess_page()
 * @see html.html.twig
 */
#}
<div id=\"page-wrapper\">
  <div id=\"page\">
    <header id=\"header\" class=\"header\" role=\"banner\" aria-label=\"{{ 'Site header'|t}}\">
      {% block head %}
        {% if page.secondary_menu or page.top_header or page.top_header_form %}
          <nav{{ navbar_top_attributes }}>
          {% if container_navbar %}
          <div class=\"container\">
          {% endif %}
              {{ page.secondary_menu }}
              {{ page.top_header }}
              {% if page.top_header_form %}
                <div class=\"form-inline navbar-form float-right\">
                  {{ page.top_header_form }}
                </div>
              {% endif %}
          {% if container_navbar %}
          </div>
          {% endif %}
          </nav>
        {% endif %}
        <nav{{ navbar_attributes }}>
          {% if container_navbar %}
          <div class=\"container\">
          {% endif %}
            {{ page.header }}
            {% if page.primary_menu or page.header_form %}
              <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#CollapsingNavbar\" aria-controls=\"CollapsingNavbar\" aria-expanded=\"false\" aria-label=\"Toggle navigation\"><span class=\"navbar-toggler-icon\"></span></button>
              <div class=\"collapse navbar-collapse\" id=\"CollapsingNavbar\">
                {{ page.primary_menu }}
                {% if page.header_form %}
                  <div class=\"form-inline navbar-form float-right\">
                    {{ page.header_form }}
                  </div>
                {% endif %}
\t          </div>
            {% endif %}
            {% if sidebar_collapse %}
              <button class=\"navbar-toggler navbar-toggler-left\" type=\"button\" data-toggle=\"collapse\" data-target=\"#CollapsingLeft\" aria-controls=\"CollapsingLeft\" aria-expanded=\"false\" aria-label=\"Toggle navigation\"></button>
            {% endif %}
          {% if container_navbar %}
          </div>
          {% endif %}
        </nav>
      {% endblock %}
    </header>
    {% if page.highlighted %}
      <div class=\"highlighted\">
        <aside class=\"{{ container }} section clearfix\" role=\"complementary\">
          {{ page.highlighted }}
        </aside>
      </div>
    {% endif %}
    {% if page.featured_top %}
      {% block featured %}
        <div class=\"featured-top\">
          <aside class=\"featured-top__inner section {{ container }} clearfix\" role=\"complementary\">
            {{ page.featured_top }}
          </aside>
        </div>
      {% endblock %}
    {% endif %}
    <div id=\"main-wrapper\" class=\"layout-main-wrapper clearfix\">
      {% block content %}
        <div id=\"main\" class=\"{{ row }}\">










          <!-- partial:index.partial.html -->
          <div class=\"skw-pages\">
            <div class=\"skw-page skw-page-1 active\">
              <div class=\"skw-page__half skw-page__half--left\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\"></div>
                </div>
              </div>
              <div class=\"skw-page__half skw-page__half--right\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\">
                    <h2 class=\"skw-page__heading\">Starter kit</h2>
                    <p class=\"skw-page__description\">
                The starter kit comprises of all what is required for production of the black soldier fly larvae. The items are as follows;
                    <ol>
                      <li>Love cage</li>
                      <li>Feeding troughs</li>
                      <li>Larvae</li>
                    </ol>
                </p>
                  </div>
                </div>
              </div>
            </div>
            <div class=\"skw-page skw-page-2\">
              <div class=\"skw-page__half skw-page__half--left\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\">
                   <!-- <h2 class=\"skw-page__heading\">Page 2</h2>-->
                    <p class=\"skw-page__description\">Once the farmer receives the full package of the starter kit they are expected to feed the larvae until they turn black. <br/>
          Once the larvae turn black the farmer needs to remove some of the waste leaving them with just some waste to give them a place where they will hide and grow further to become the fly. </p>
                  </div>
                </div>
              </div>
              <div class=\"skw-page__half skw-page__half--right\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\"></div>
                </div>
              </div>
            </div>
            <div class=\"skw-page skw-page-3\">
              <div class=\"skw-page__half skw-page__half--left\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\"></div>
                </div>
              </div>
              <div class=\"skw-page__half skw-page__half--right\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\">
               <!--     <h2 class=\"skw-page__heading\">Page 3</h2> -->
                    <p class=\"skw-page__description\">During this time the farmer should cover the container with a net to ensure that once any fly emerges it remains there.<br/>
          Once you start having flies you need to move the container to the love cage where they will continue to pupate turning into flies.
          </p>
                  </div>
                </div>
              </div>
            </div>
            <div class=\"skw-page skw-page-4\">
              <div class=\"skw-page__half skw-page__half--left\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\">
              <!--      <h2 class=\"skw-page__heading\">Page 4</h2> -->
                    <p class=\"skw-page__description\">After every three days you need to collect the carton pieces and place on top of a trough with some food waste give them four days and they will have hatched.
                <br/>
                Once they are 5 days old start feeding them with each trough doing 1.5 kgs of waste and on the fourth day from when you feed them. <br/> Feed them more with 2.5 kgs of waste and on the eighth day feed them with 4kgs.<br/> From that point give them more time to feed and mature.

                </p>
                  </div>
                </div>
              </div>
              <div class=\"skw-page__half skw-page__half--right\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\"></div>
                </div>
              </div>
            </div>
            <div class=\"skw-page skw-page-5\">
              <div class=\"skw-page__half skw-page__half--left\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\"></div>
                </div>
              </div>
              <div class=\"skw-page__half skw-page__half--right\">
                <div class=\"skw-page__skewed\">
                  <div class=\"skw-page__content\">
                   <!-- <h2 class=\"skw-page__heading\">Epic finale</h2> -->
                    <p class=\"skw-page__description\">
                      From that point you can feed them to the chicken, pig or fish with the waste remaining feeding it to your plants as organic fertilizer.

                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- partial -->

          <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>










        </div>
      {% endblock %}
    </div>
    {% if page.featured_bottom_first or page.featured_bottom_second or page.featured_bottom_third %}
      <div class=\"featured-bottom\">
        <aside class=\"{{ container }} clearfix\" role=\"complementary\">
          {{ page.featured_bottom_first }}
          {{ page.featured_bottom_second }}
          {{ page.featured_bottom_third }}
        </aside>
      </div>
    {% endif %}
    <footer class=\"site-footer\">
      {% block footer %}
        <div class=\"{{ container }}\">
          {% if page.footer_first or page.footer_second or page.footer_third or page.footer_fourth %}
            <div class=\"site-footer__top clearfix\">
              {{ page.footer_first }}
              {{ page.footer_second }}
              {{ page.footer_third }}
              {{ page.footer_fourth }}
            </div>
          {% endif %}
          {% if page.footer_fifth %}
            <div class=\"site-footer__bottom\">
              {{ page.footer_fifth }}
            </div>
          {% endif %}
        </div>
      {% endblock %}
    </footer>
  </div>
</div>
", "themes/bootstrap_barrio/subtheme/templates/pages/page--starter-kit.html.twig", "/var/www/Ressect4farmers/web/themes/bootstrap_barrio/subtheme/templates/pages/page--starter-kit.html.twig");
    }
}
