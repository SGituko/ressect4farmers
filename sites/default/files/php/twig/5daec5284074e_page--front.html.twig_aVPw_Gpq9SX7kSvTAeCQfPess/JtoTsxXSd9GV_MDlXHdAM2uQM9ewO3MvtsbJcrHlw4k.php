<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/bootstrap_barrio/subtheme/templates/custom/page--front.html.twig */
class __TwigTemplate_34461fd34cd4beb2957b998ddc3a67e30fd10260b12951b2bfa4001f60b60bf4 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'featured' => [$this, 'block_featured'],
            'footer' => [$this, 'block_footer'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["block" => 73, "if" => 219];
        $filters = ["t" => 72, "escape" => 221];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['block', 'if'],
                ['t', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453 = $this->env->getExtension("Drupal\\webprofiler\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->enter($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "themes/bootstrap_barrio/subtheme/templates/custom/page--front.html.twig"));

        // line 70
        echo "<div id=\"page-wrapper\">
  <div id=\"page\">
    <header id=\"header\" class=\"header\" role=\"banner\" aria-label=\"";
        // line 72
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Site header"));
        echo "\">
      ";
        // line 73
        $this->displayBlock('head', $context, $blocks);
        // line 115
        echo "    </header>


    <!--  Full width slider partial:index.partial.html -->
    <div class=\"slider-container\">
      <div class=\"slider-control left inactive\"></div>
      <div class=\"slider-control right\"></div>
      <ul class=\"slider-pagi\"></ul>
      <div class=\"slider\">
        <div class=\"slide slide-0 active\">
          <div class=\"slide__bg\"></div>
          <div class=\"slide__content\">
            <svg class=\"slide__overlay\" viewBox=\"0 0 720 405\" preserveAspectRatio=\"xMaxYMax slice\">
              <path class=\"slide__overlay-path\" d=\"M0,0 150,0 500,405 0,405\" />
            </svg>
            <div class=\"slide__text\">
              <h2 class=\"slide__text-heading\">Why insect Farming?</h2>
              <p class=\"slide__text-desc\">Did you know that wild birds and free-range poultry can consume insects inform an adult,
                 larval and pupal naturally? Register now to learn how you can use insects as feed supplement for your livestock.</p>
            <!--  <a class=\"slide__text-link\"></a>-->
            </div>
          </div>
        </div>
        <div class=\"slide slide-1 \">
          <div class=\"slide__bg\"></div>
          <div class=\"slide__content\">
            <svg class=\"slide__overlay\" viewBox=\"0 0 720 405\" preserveAspectRatio=\"xMaxYMax slice\">
              <path class=\"slide__overlay-path\" d=\"M0,0 150,0 500,405 0,405\" />
            </svg>
            <div class=\"slide__text\">
              <h2 class=\"slide__text-heading\">Why Black Soldier Fly?</h2>
              <p class=\"slide__text-desc\">Unlike House Flies, Black Soldier Flies are not known to transmit diseases.
Insect that can turn farm waste into rich animal feed. This providing a valuable alternative to conventional yet expensive protein feed.
</p>

            </div>
          </div>
        </div>
        <div class=\"slide slide-2\">
          <div class=\"slide__bg\"></div>
          <div class=\"slide__content\">
            <svg class=\"slide__overlay\" viewBox=\"0 0 720 405\" preserveAspectRatio=\"xMaxYMax slice\">
              <path class=\"slide__overlay-path\" d=\"M0,0 150,0 500,405 0,405\" />
            </svg>
            <div class=\"slide__text\">
              <h2 class=\"slide__text-heading\">WASTE MANAGEMENT</h2>
              <p class=\"slide__text-desc\">We all generate a lot of waste every day in our kitchens,
                we dispose most of it but for a wise framer will use it to feed insects that will in turn feed his/her poultry, fish and pigs.”</p>

            </div>
          </div>
        </div>
        <div class=\"slide slide-3\">
          <div class=\"slide__bg\"></div>
          <div class=\"slide__content\">
            <svg class=\"slide__overlay\" viewBox=\"0 0 720 405\" preserveAspectRatio=\"xMaxYMax slice\">
              <path class=\"slide__overlay-path\" d=\"M0,0 150,0 500,405 0,405\" />
            </svg>
            <div class=\"slide__text\">
              <h2 class=\"slide__text-heading\">WHY JOIN RESSECT?</h2>
              <p class=\"slide__text-desc\">You will be part of an open exchange about and discussion of the
                practical day-to-day working steps required to operate a productive Black Soldier Fly facility.</p>

            </div>
          </div>
        </div>







      </div>
    </div>
    <!-- partial -->

 <!--
<div class=\"jumbotron jumbotronFront masthead\" id=\"home\">
  <div class=\"container jumbotronText\">
    <h1 style=\"background: black;display: -webkit-inline-box;padding: 5px;\">Ressect4farmers</h1>
    <p style=\"background: black;display: -webkit-inline-box;padding: 5px;\">Sleek, smooth, powerful, and revolutionary jQuery lightbox plugin for creative and ambitious web designers and developers.</p>


    <p>
      <a href=\"https://goo.gl/nl0Bg\" class=\"btn btn-primary btn-large\">Download iLightBox&nbsp;&nbsp;<i class=\"icon-download-alt icon-white\"></i></a>
      <a href=\"https://goo.gl/DlaJq\" class=\"btn btn-info btn-large\">Download for WordPress&nbsp;&nbsp;<i class=\"icon-download-alt icon-white\"></i></a>
      <a href=\"discussion.html\" class=\"btn btn-success btn-large\">Discussion&nbsp;&nbsp;<i class=\"icon-comment icon-white\"></i></a>
    </p>
   -->
   <!--
    <ul class=\"masthead-links\">
      <li>

      </li>
    </ul>
    -->
  </div>
</div>





    ";
        // line 219
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 220
            echo "      <div class=\"highlighted\">
        <aside class=\"";
            // line 221
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
            echo " section clearfix\" role=\"complementary\">
          ";
            // line 222
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
            echo "
        </aside>
      </div>
    ";
        }
        // line 226
        echo "    ";
        if ($this->getAttribute(($context["page"] ?? null), "featured_top", [])) {
            // line 227
            echo "      ";
            $this->displayBlock('featured', $context, $blocks);
            // line 234
            echo "    ";
        }
        // line 235
        echo "
















    <!-- partial:index.partial.html -->
    <div class=\"cards\" style=\"background: aliceblue; padding-top: 4em;padding-bottom: 10em;\">


  <!--    <div class=\"introExplanationSec\"> -->
        <h3 style=\"text-align:center;padding-bottom: 5px;color: #000;text-decoration: overline;\">How It Works</h3>
      <!--  <p style=\"text-align:center;color: #000;text-decoration: overline; font-weight: 600;\">Are you a farmer and interested in making extra cash with your kitchen or agricultural waste?
          <ol style=\"text-align:center;color: #000; font-weight: 400;\">
            <li>You want to stop spending money on chemical fertilizer</li>
            <li>You want to lower your risk of being dependent on a single crop harvest</li>
            <li>You want to make an extra income out of it</li>
          </ol>

        </p>
      </div>-->


      <div class=\"card\">
        <img class=\"card__image\" src=\" ";
        // line 270
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null)), "html", null, true);
        echo "/images/how-to/1.jpg\" alt=\"wave\" />
        <div class=\"card-title\" style=\"margin-bottom:0\">
          <a href=\"#\" class=\"toggle-info btn\">
            <span class=\"left\"></span>
            <span class=\"right\"></span>
          </a>
          <h2>
              1. Register with us</br></br>
              <!--<small>Image from unsplash.com</small>-->
          </h2>
        </div>
        <div class=\"card-flap flap1\">
          <div class=\"card-description\">
          Register with us today and get free unlimited access to our knowledge center
          and our forum where you will get open exchange about and discussion of the practical
           day-to-day working steps required to operate a Black Soldier Fly facility form researchers and farmers alike.
          </div>
        <!--  <div class=\"card-flap flap2\">
            <div class=\"card-actions\">
              <a href=\"#\" class=\"btn\">Read more</a>
            </div>
          </div> -->
        </div>
      </div>

        <div class=\"card\">
        <img class=\"card__image\" src=\"";
        // line 296
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null)), "html", null, true);
        echo "/images/how-to/2.jpg\" alt=\"beach\" />
        <div class=\"card-title\" style=\"margin-bottom:0\">
          <a href=\"#\" class=\"toggle-info btn\">
            <span class=\"left\"></span>
            <span class=\"right\"></span>
          </a>
          <h2>
              2. Learn on knowledge center</br></br>
            <!--  <small>Image from unsplash.com</small> -->
          </h2>
        </div>
        <div class=\"card-flap flap1\">
          <div class=\"card-description\">
            There is no need for sophisticated high-end technology to start a Black Soildier Fly Facility or Skilled Labour.
             Get access to our Knowlegde centre anywhere, anytime and Begin Today.
          </div>
      <!--    <div class=\"card-flap flap2\">
            <div class=\"card-actions\">
              <a href=\"#\" class=\"btn\">Read more</a>
            </div>
          </div> -->
        </div>
      </div>

      <div class=\"card\">
        <img class=\"card__image\" src=\"";
        // line 321
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null)), "html", null, true);
        echo "/images/how-to/3.jpg\" alt=\"mountain\" />
        <div class=\"card-title\" style=\"margin-bottom:0\">
          <a href=\"#\" class=\"toggle-info btn\">
            <span class=\"left\"></span>
            <span class=\"right\"></span>
          </a>
          <h2>
            3. Order the specialized starter kit
            <!--  <small>Image from unsplash.com</small>-->
          </h2>
        </div>
        <div class=\"card-flap flap1\">
          <div class=\"card-description\">
          Insect farming uses a tiny fraction of the feed, water, land and labor needed to raise traditional
          livestock such as cattle or pigs. Why not start today with a BST STARTER KIT from as low as 0000 Kshs.
          </div>
        <!--  <div class=\"card-flap flap2\">
            <div class=\"card-actions\">
              <a href=\"#\" class=\"btn\">Read more</a>
            </div>
          </div> -->
        </div>
      </div>

      <div class=\"card\">
        <img class=\"card__image\" src=\"";
        // line 346
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null)), "html", null, true);
        echo "/images/how-to/4.jpg\" alt=\"field\" />
        <div class=\"card-title\" style=\"margin-bottom:0\">
          <a href=\"#\" class=\"toggle-info btn\">
            <span class=\"left\"></span>
            <span class=\"right\"></span>
          </a>
          <h2>
              4. Sell on our digital market place
            <!--  <small>Image from unsplash.com</small> -->
          </h2>
        </div>
        <div class=\"card-flap flap1\">
          <div class=\"card-description\">
            Easy-to-use. Quick go-to-market with Over 500 farmers visit this page every day.
            Choose from thousands of great products to connect buyers and sellers with the all-in one platform.
            Currently, you can buy or sell a kilo of dried larvae at between Sh35 and Sh40.
          </div>
      <!--    <div class=\"card-flap flap2\">
            <div class=\"card-actions\">
              <a href=\"#\" class=\"btn\">Read more</a>
            </div>
          </div> -->
        </div>
      </div>
    </div>
    <!-- partial -->
      <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js'></script>


































<!--
<div class=\"section\" style=\"/* background: #435964c7; */padding-top: 50px;padding-bottom: 50px;background: rgba(0, 0, 0, 0.14);\">
<div class=\"introExplanationSec\">
  <h3 style=\"text-align:center;padding-bottom: 30px;color: #000;text-decoration: overline;\">How It Works</h3>
  <p style=\"text-align:center;color: #000;text-decoration: overline; font-weight: 600;\">Are you a farmer and interested in making extra cash with your kitchen or agricultural waste?
    <ol style=\"text-align:center;color: #000; font-weight: 400;\">
      <li>You want to stop spending money on chemical fertilizer</li>
      <li>You want to lower your risk of being dependent on a single crop harvest</li>
      <li>You want to make an extra income out of it</li>
    </ol>

  </p>
</div>


   <div class=\"container\">
      <div class=\"row align-items-stretch\">
         <div class=\"col-lg-4 order-lg-2\" style=\" padding-bottom: 30px; \">
            <div class=\"scaling-image h-100\">
               <div class=\"frame h-100\">
                  <div class=\"feature-img-bg h-100\" style=\"\">
                  </div>
               </div>
            </div>
         </div>
         <div class=\"col-md-6 col-lg-4 feature-1-wrap d-md-flex flex-md-column order-lg-1\" style=\" padding-bottom: 30px; \">
            <div class=\"feature-1 d-md-flex\">
               <div class=\"align-self-center\">
                  <span class=\"flaticon-chakra display-4 text-primary\"></span>
                  <h5>1. Register Here</h5>
                  <p>Register on this website and receive free access to the Knowledge Center, StarterKit ordering and Digital marketplace.
                  </br></br>
                  Register now and get updates as soon as the platform is finalized
                  </p>

               </div>
            </div>
            <div class=\"feature-1 d-md-flex\">
               <div class=\"align-self-center\">
                  <span class=\"flaticon-lotus display-4 text-primary\"></span>
                  <h5>3. Order the specialized StarterKit</h5>
                  <p>After having easily acquired knowledge
                    you have the possibility to order a dedicated StarterKit
                    allowing you to optimally produce your insects.</p>
               </div>
            </div>
         </div>
         <div class=\"col-md-6 col-lg-4 feature-1-wrap d-md-flex flex-md-column order-lg-3\" style=\" padding-bottom: 30px; \">
            <div class=\"feature-1 d-md-flex\">
               <div class=\"align-self-center\">
                  <span class=\"flaticon-chakra-1 display-4 text-primary\"></span>
                  <h5>2. Learn on Knowledge Center</h5>
                  <p>Get the full knowledge and every information around insect production for free. Enroll on lectures, watch learning videos and read the articles.</p>
               </div>
            </div>
            <div class=\"feature-1 d-md-flex\">
               <div class=\"align-self-center\">
                  <span class=\"flaticon-yoga display-4 text-primary\"></span>
                  <h5>4. Sell on digital marketplace</h5>
                  <p>Take advantage of the easiest way to sell your products via the \"marketplace\" section on the website. On the marketplace you can find local aquaculture, poultry and pig farmers and sell directly via the website.</p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div> -->

<!--Categories-->

<script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>

<section id=\"section-feature\" class=\"row\">
    <div class=\"container\">
        <ul>
            <li id=\"sf-innovation\" class=\"col-md-3 col-sm-6 col-xs-12\">
                <div class=\"sf-wrap\">

                    <div class=\"sf-mdl-left\">

                        <div class=\"sf-icon\">

                            <i class=\"fa fa-fw fa-book fa-5x\"></i>
                        </div>
                        <h3>Ressect Resource</h3>

                    </div>
                    <div class=\"sf-mdl-right\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-book fa-5x\"></i>
                        </div>
                        <h3>Ressect Resource</h3>

                    </div>

                    <div class=\"sf-mdl-left-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-book fa-5x\"></i>
                        </div>

                        <h3><a href=\"#\">Ressect Resource</a>
                        </h3>
                        <p>There is no need for sophisticated high-end technology to start a Black Soildier Fly Facility or Skilled Labour.
                          Get access to our Knowlegde centre anywhere, anytime and Begin Today.</p>

                    </div>
                    <div class=\"sf-mdl-right-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-book fa-5x\"></i>
                        </div>
                        <h3><a href=\"#\">Ressect Resource</a></h3>
                        <p>Curabitur blandit tempus ardua ridiculus sed magna. Integer legentibus erat a ante historiarum dapibus.</p>
                    </div>
                </div>
            </li>

            <li id=\"sf-community\" class=\"col-md-3 col-sm-6 col-xs-12\">
                <div class=\"sf-wrap\">
                    <div class=\"sf-mdl-left\">
                        <div class=\"sf-icon\">

                            <i class=\"fa fa-fw fa-shopping-basket fa-5x\"></i>
                        </div>
                        <h3>Ressect Marketplace</h3>
                    </div>
                    <div class=\"sf-mdl-right\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-shopping-basket fa-5x\"></i>
                        </div>
                        <h3>Ressect Marketplace</h3>
                    </div>

                    <div class=\"sf-mdl-left-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-shopping-basket fa-5x\"></i>
                        </div>
                        <h3><a href=\"#\">Ressect Marketplace</a></h3>
                        <p>Easy-to-use. Quick go-to-market with Over 500 farmers visit this page everyday. Choose from thousands of great products to connect buyers and sellers with the all-in one platform </p>
                    </div>
                    <div class=\"sf-mdl-right-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-shopping-basket fa-5x\"></i>
                        </div>
                        <h3><a href=\"#\">Ressect Marketplace</a></h3>
                        <p>Easy-to-use. Quick go-to-market with Over 500 farmers visit this page everyday. Choose from thousands of great products to connect buyers and sellers with the all-in one platform </p>
                    </div>
                </div>
            </li>

            <li id=\"sf-academy\" class=\"col-md-3 col-sm-6 col-xs-12\">
                <div class=\"sf-wrap\">
                    <div class=\"sf-mdl-left\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-info fa-5x\"></i>
                        </div>
                        <h3>Starter kit</h3>
                    </div>
                    <div class=\"sf-mdl-right\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-info fa-5x\"></i>
                        </div>
                        <h3>Starter kit</h3>
                    </div>

                    <div class=\"sf-mdl-left-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-info fa-5x\"></i>
                        </div>
                        <h3><a href=\"#\">Starter kit</a></h3>
                        <p>Starter Kit has all of the basic necessities required to get you started. It contains Explain the items.</p>
                    </div>
                    <div class=\"sf-mdl-right-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-info fa-5x\"></i>
                        </div>
                        <h3><a href=\"#\">Starter kit</a></h3>
                        <p>Starter Kit has all of the basic necessities required to get you started. It contains Explain the items.</p>
                    </div>
                </div>
            </li>

            <li id=\"sf-academy\" class=\"col-md-3 col-sm-6 col-xs-12\">
                <div class=\"sf-wrap\">
                    <div class=\"sf-mdl-left\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-comments fa-5x\"></i>
                        </div>
                        <h3>Forum</h3>
                    </div>
                    <div class=\"sf-mdl-right\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-comments fa-5x\"></i>
                        </div>
                        <h3>Forum</h3>
                    </div>

                    <div class=\"sf-mdl-left-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-comments fa-5x\"></i>
                        </div>
                        <h3><a href=\"#\">Forum</a></h3>
                        <p>This community is ready to help with answers to your questions and ongoing support.</p>
                    </div>
                    <div class=\"sf-mdl-right-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-comments fa-5x\"></i>
                        </div>
                        <h3><a href=\"#\">Forum</a></h3>
                        <p>This community is ready to help with answers to your questions and ongoing support.</p>
                    </div>
                </div>
            </li>


                <div class=\"sf-wrap\">
                    <div class=\"sf-mdl-left\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-star-half-o fa-5x\"></i>
                        </div>
                        <h3>Source File Included</h3>
                    </div>
                    <div class=\"sf-mdl-right\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-star-half-o fa-5x\"></i>
                        </div>
                        <h3>Source File Included</h3>
                    </div>

                    <div class=\"sf-mdl-left-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-star fa-5x\"></i>
                        </div>
                        <h3><a href=\"#\">Source File Included</a></h3>
                        <p>Morbi fringilla convallis sapien, id pulvinar odio volutpat. Contra legem facit qui id facit quod lex prohibet.</p>
                    </div>
                    <div class=\"sf-mdl-right-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-star fa-5x\"></i>
                        </div>
                        <h3><a href=\"#\">Source File Included</a></h3>
                        <p>Morbi fringilla convallis sapien, id pulvinar odio volutpat. Contra legem facit qui id facit quod lex prohibet.</p>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>




























<!--Parteners-->


<div class=\"gradient-container\">
<h1 style=\"position:absolute;left:44%;color:#fff; padding-top: 10px;\">Partners </h1>
    <div class=\"text row justify-content-md-center\">


  <div class=\"col col-lg-2\" style=\" margin: 20px; \">
  <div class=\"partner-box\">
   <img src=\"themes/bootstrap_barrio/subtheme/images/repic-logo.png\" style=\"max-width: 90px; margin-top: 10px;\"/>
   <span class=\"partner-head\" style=\"font-size: 50px;font-weight: 500;color: #f1a658;\">REPIC


   </span></div></div>
   <div class=\"col-md-auto\" style=\"margin:20px;font-size:1.2em; font-weight:700;\">
          <ul style=\"list-style: none;\">
            <li>Schweizerische Eidgenossenschaft</li>
            <li>Confèdèration suisse</li>
            <li>Confederazione Svizzera</li>
            <li>Confederaziun svizra</li>
          </ul>
</div>


        <div class=\"col-lg-5\" style=\"margin:20px;font-size:1.2em;\">
          <ul style=\"list-style: none;\">
            <li>State Secretariat for Economic Affairs SECO</li>
            <li>Swiss Agency for Development and Cooperation SDC</li>
            <li>Federal Office for the Environment FOEN</li>
            <li>Swiss Federal Office of Energy SFOE</li>
          </ul>
      </div>
  </div>
  <!-- SVG Grid Background Start    -->
  <div class=\"grid-background\">
    <svg width=\"100vw\" height=\"100vh\" xmlns=\"http://www.w3.org/2000/svg\">
    <defs>

      <pattern id=\"grid\" width=\"20\" height=\"20\" patternUnits=\"userSpaceOnUse\">
        <rect width=\"80\" height=\"80\" fill=\"url(#smallGrid)\"/>
        <path d=\"M 80 0 L 0 0 0 80\" fill=\"none\" stroke=\"white\" stroke-width=\"1\"/>
      </pattern>
    </defs>

    <rect width=\"100%\" height=\"100%\" fill=\"url(#grid)\" />
  </svg>
  </div>
  <!--SVG Grid Background End    -->
  <!-- Animation Start    -->
  <div class=\"slide-lines\">
    <!-- HORIZONTAL LINES -->
    <div class=\"line hz-line\"></div>
    <div class=\"line hz-line\"></div>
    <div class=\"line hz-line\"></div>
    <!-- VERTICAL LINES -->
    <div class=\"line vert-line\"></div>
    <div class=\"line vert-line\"></div>
    <div class=\"line vert-line\"></div>
  <!--Animation End  -->
</div>

</div>






  <!--Main section-->
  ";
        // line 784
        echo "









          ";
        // line 794
        if ((($this->getAttribute(($context["page"] ?? null), "featured_bottom_first", []) || $this->getAttribute(($context["page"] ?? null), "featured_bottom_second", [])) || $this->getAttribute(($context["page"] ?? null), "featured_bottom_third", []))) {
            // line 795
            echo "      <div class=\"featured-bottom\">
        <aside class=\"";
            // line 796
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
            echo " clearfix\" role=\"complementary\">
          ";
            // line 797
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "featured_bottom_first", [])), "html", null, true);
            echo "
          ";
            // line 798
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "featured_bottom_second", [])), "html", null, true);
            echo "
          ";
            // line 799
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "featured_bottom_third", [])), "html", null, true);
            echo "
        </aside>
      </div>
    ";
        }
        // line 803
        echo "

    <footer class=\"site-footer\" style=\"height: 100%;background-position: center;background-repeat: no-repeat;background-size: cover;\">


    <!--
    <div class=\"container-fluid\" id=\"footerSuga\">
      <div class=\"row\">

          <div class=\"col-md-3 leftSpace\">
            <div><img src=\"themes/bootstrap_barrio/subtheme/images/logo.png\" style=\"max-width: 130px; margin-top: 10px;\"/></div>
          </div>
          <div class=\"col-md-6 leftSpace\" style=\"padding: 20px;\">

            <iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/QGgcoZKympI\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
          </div>


          <div class=\"col-md-3 leftSpace\">


            <script async src=\"https://static.addtoany.com/menu/page.js\"></script>
            <!-- AddToAny END
            <h5>About Us</h5>
            <h5>About Us</h5>
            <h5>About Us</h5>

            <!-- AddToAny BEGIN
            <div class=\"a2a_kit a2a_kit_size_32 a2a_default_style\" style=\"margin-top:50px;\">
                <a class=\"a2a_button_facebook\"></a>
                <a class=\"a2a_button_twitter\"></a>
                <a class=\"a2a_button_google_plus\"></a>
            </div>

             <h5>Ressect GmbH</br>
                Im Werd 6</br>
                8952 Schlieren</h5>
          </div>

          <div class=\"container\" style=\"max-width: 980px;\">
          <p style=\"font-family: cursive;\">
              Ressec4farmers is a project from a Swiss animal feed company and
              together with Egerton University we are running an agricultural project in Nakuru County.

            Our project aims to teach local farmers the science of insect farming.
            Farmers can use their agricultural wastes to grow insects which they then can in turn feed to
             their livestock animals. We want to offer farmers in Nakuru an online course on how to produce insects.

            Insect farming is not only good for the environment but is also improving the lives of many farmers in the world.
             We believe that this platform will positively impact farmers in Nakuru County.
          </p>
          </div>

    </div>
  </div> -->







      ";
        // line 865
        $this->displayBlock('footer', $context, $blocks);
        // line 882
        echo "



      <div style=\" color: white; text-align: -webkit-center; font-size: 0.8em; padding-top: 20px;\">Ressect4farmers &copy; 2019</div>
    </footer>
  </div>
</div>
";
        
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->leave($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof);

    }

    // line 73
    public function block_head($context, array $blocks = [])
    {
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453 = $this->env->getExtension("Drupal\\webprofiler\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->enter($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "head"));

        // line 74
        echo "        ";
        if ((($this->getAttribute(($context["page"] ?? null), "secondary_menu", []) || $this->getAttribute(($context["page"] ?? null), "top_header", [])) || $this->getAttribute(($context["page"] ?? null), "top_header_form", []))) {
            // line 75
            echo "          <nav";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["navbar_top_attributes"] ?? null)), "html", null, true);
            echo ">
          ";
            // line 76
            if (($context["container_navbar"] ?? null)) {
                // line 77
                echo "          <div class=\"container\">
          ";
            }
            // line 79
            echo "              ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "secondary_menu", [])), "html", null, true);
            echo "
              ";
            // line 80
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "top_header", [])), "html", null, true);
            echo "
              ";
            // line 81
            if ($this->getAttribute(($context["page"] ?? null), "top_header_form", [])) {
                // line 82
                echo "                <div class=\"form-inline navbar-form float-right\">
                  ";
                // line 83
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "top_header_form", [])), "html", null, true);
                echo "
                </div>
              ";
            }
            // line 86
            echo "          ";
            if (($context["container_navbar"] ?? null)) {
                // line 87
                echo "          </div>
          ";
            }
            // line 89
            echo "          </nav>
        ";
        }
        // line 91
        echo "        <nav";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["navbar_attributes"] ?? null)), "html", null, true);
        echo " class=\"navbar navbar-inverse bg-inverse navbar-toggleable-sm fixed-top\">
          ";
        // line 92
        if (($context["container_navbar"] ?? null)) {
            // line 93
            echo "          <div class=\"container\">
          ";
        }
        // line 95
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
        echo "
            ";
        // line 96
        if (($this->getAttribute(($context["page"] ?? null), "primary_menu", []) || $this->getAttribute(($context["page"] ?? null), "header_form", []))) {
            // line 97
            echo "              <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#CollapsingNavbar\" aria-controls=\"CollapsingNavbar\" aria-expanded=\"false\" aria-label=\"Toggle navigation\"><span class=\"navbar-toggler-icon\">Menu</span></button>
              <div class=\"collapse navbar-collapse\" id=\"CollapsingNavbar\">
                ";
            // line 99
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "primary_menu", [])), "html", null, true);
            echo "
                ";
            // line 100
            if ($this->getAttribute(($context["page"] ?? null), "header_form", [])) {
                // line 101
                echo "                  <div class=\"form-inline navbar-form float-right\">
                    ";
                // line 102
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header_form", [])), "html", null, true);
                echo "
                  </div>
                ";
            }
            // line 105
            echo "\t          </div>
            ";
        }
        // line 107
        echo "            ";
        if (($context["sidebar_collapse"] ?? null)) {
            // line 108
            echo "              <button class=\"navbar-toggler navbar-toggler-left\" type=\"button\" data-toggle=\"collapse\" data-target=\"#CollapsingLeft\" aria-controls=\"CollapsingLeft\" aria-expanded=\"false\" aria-label=\"Toggle navigation\"></button>
            ";
        }
        // line 110
        echo "          ";
        if (($context["container_navbar"] ?? null)) {
            // line 111
            echo "          </div>
          ";
        }
        // line 113
        echo "        </nav>
      ";
        
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->leave($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof);

    }

    // line 227
    public function block_featured($context, array $blocks = [])
    {
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453 = $this->env->getExtension("Drupal\\webprofiler\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->enter($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "featured"));

        // line 228
        echo "        <div class=\"featured-top\">
          <aside class=\"featured-top__inner section ";
        // line 229
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo " clearfix\" role=\"complementary\">
            ";
        // line 230
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "featured_top", [])), "html", null, true);
        echo "
          </aside>
        </div>
      ";
        
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->leave($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof);

    }

    // line 865
    public function block_footer($context, array $blocks = [])
    {
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453 = $this->env->getExtension("Drupal\\webprofiler\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->enter($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "footer"));

        // line 866
        echo "        <div class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo "\">
          ";
        // line 867
        if (((($this->getAttribute(($context["page"] ?? null), "footer_first", []) || $this->getAttribute(($context["page"] ?? null), "footer_second", [])) || $this->getAttribute(($context["page"] ?? null), "footer_third", [])) || $this->getAttribute(($context["page"] ?? null), "footer_fourth", []))) {
            // line 868
            echo "            <div class=\"site-footer__top clearfix\">
              ";
            // line 869
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_first", [])), "html", null, true);
            echo "
              ";
            // line 870
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_second", [])), "html", null, true);
            echo "
              ";
            // line 871
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_third", [])), "html", null, true);
            echo "
              ";
            // line 872
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_fourth", [])), "html", null, true);
            echo "
            </div>
          ";
        }
        // line 875
        echo "          ";
        if ($this->getAttribute(($context["page"] ?? null), "footer_fifth", [])) {
            // line 876
            echo "            <div class=\"site-footer__bottom\">
              ";
            // line 877
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_fifth", [])), "html", null, true);
            echo "
            </div>
          ";
        }
        // line 880
        echo "        </div>
      ";
        
        $__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453->leave($__internal_b8a44bb7188f10fa054f3681425c559c29de95cd0490f5c67a67412aafc0f453_prof);

    }

    public function getTemplateName()
    {
        return "themes/bootstrap_barrio/subtheme/templates/custom/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1034 => 880,  1028 => 877,  1025 => 876,  1022 => 875,  1016 => 872,  1012 => 871,  1008 => 870,  1004 => 869,  1001 => 868,  999 => 867,  994 => 866,  988 => 865,  977 => 230,  973 => 229,  970 => 228,  964 => 227,  956 => 113,  952 => 111,  949 => 110,  945 => 108,  942 => 107,  938 => 105,  932 => 102,  929 => 101,  927 => 100,  923 => 99,  919 => 97,  917 => 96,  912 => 95,  908 => 93,  906 => 92,  901 => 91,  897 => 89,  893 => 87,  890 => 86,  884 => 83,  881 => 82,  879 => 81,  875 => 80,  870 => 79,  866 => 77,  864 => 76,  859 => 75,  856 => 74,  850 => 73,  835 => 882,  833 => 865,  769 => 803,  762 => 799,  758 => 798,  754 => 797,  750 => 796,  747 => 795,  745 => 794,  733 => 784,  324 => 346,  296 => 321,  268 => 296,  239 => 270,  202 => 235,  199 => 234,  196 => 227,  193 => 226,  186 => 222,  182 => 221,  179 => 220,  177 => 219,  71 => 115,  69 => 73,  65 => 72,  61 => 70,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Bootstrap Barrio's theme implementation to display a single page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.html.twig template normally located in the
 * core/modules/system directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - base_path: The base URL path of the Drupal installation. Will usually be
 *   \"/\" unless you have installed Drupal in a sub-directory.
 * - is_front: A flag indicating if the current page is the front page.
 * - logged_in: A flag indicating if the user is registered and signed in.
 * - is_admin: A flag indicating if the user has permission to access
 *   administration pages.
 *
 * Site identity:
 * - front_page: The URL of the front page. Use this instead of base_path when
 *   linking to the front page. This includes the language domain or prefix.
 * - logo: The url of the logo image, as defined in theme settings.
 * - site_name: The name of the site. This is empty when displaying the site
 *   name has been disabled in the theme settings.
 * - site_slogan: The slogan of the site. This is empty when displaying the site
 *   slogan has been disabled in theme settings.

 * Page content (in order of occurrence in the default page.html.twig):
 * - node: Fully loaded node, if there is an automatically-loaded node
 *   associated with the page and the node ID is the second argument in the
 *   page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - page.top_header: Items for the top header region.
 * - page.top_header_form: Items for the top header form region.
 * - page.header: Items for the header region.
 * - page.header_form: Items for the header form region.
 * - page.highlighted: Items for the highlighted region.
 * - page.primary_menu: Items for the primary menu region.
 * - page.secondary_menu: Items for the secondary menu region.
 * - page.featured_top: Items for the featured top region.
 * - page.content: The main content of the current page.
 * - page.sidebar_first: Items for the first sidebar.
 * - page.sidebar_second: Items for the second sidebar.
 * - page.featured_bottom_first: Items for the first featured bottom region.
 * - page.featured_bottom_second: Items for the second featured bottom region.
 * - page.featured_bottom_third: Items for the third featured bottom region.
 * - page.footer_first: Items for the first footer column.
 * - page.footer_second: Items for the second footer column.
 * - page.footer_third: Items for the third footer column.
 * - page.footer_fourth: Items for the fourth footer column.
 * - page.footer_fifth: Items for the fifth footer column.
 * - page.breadcrumb: Items for the breadcrumb region.
 *
 * Theme variables:
 * - navbar_top_attributes: Items for the header region.
 * - navbar_attributes: Items for the header region.
 * - content_attributes: Items for the header region.
 * - sidebar_first_attributes: Items for the highlighted region.
 * - sidebar_second_attributes: Items for the primary menu region.
 * - sidebar_collapse: If the sidebar_first will collapse.
 *
 * @see template_preprocess_page()
 * @see bootstrap_barrio_preprocess_page()
 * @see html.html.twig
 */
#}
<div id=\"page-wrapper\">
  <div id=\"page\">
    <header id=\"header\" class=\"header\" role=\"banner\" aria-label=\"{{ 'Site header'|t}}\">
      {% block head %}
        {% if page.secondary_menu or page.top_header or page.top_header_form %}
          <nav{{ navbar_top_attributes }}>
          {% if container_navbar %}
          <div class=\"container\">
          {% endif %}
              {{ page.secondary_menu }}
              {{ page.top_header }}
              {% if page.top_header_form %}
                <div class=\"form-inline navbar-form float-right\">
                  {{ page.top_header_form }}
                </div>
              {% endif %}
          {% if container_navbar %}
          </div>
          {% endif %}
          </nav>
        {% endif %}
        <nav{{ navbar_attributes }} class=\"navbar navbar-inverse bg-inverse navbar-toggleable-sm fixed-top\">
          {% if container_navbar %}
          <div class=\"container\">
          {% endif %}
            {{ page.header }}
            {% if page.primary_menu or page.header_form %}
              <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#CollapsingNavbar\" aria-controls=\"CollapsingNavbar\" aria-expanded=\"false\" aria-label=\"Toggle navigation\"><span class=\"navbar-toggler-icon\">Menu</span></button>
              <div class=\"collapse navbar-collapse\" id=\"CollapsingNavbar\">
                {{ page.primary_menu }}
                {% if page.header_form %}
                  <div class=\"form-inline navbar-form float-right\">
                    {{ page.header_form }}
                  </div>
                {% endif %}
\t          </div>
            {% endif %}
            {% if sidebar_collapse %}
              <button class=\"navbar-toggler navbar-toggler-left\" type=\"button\" data-toggle=\"collapse\" data-target=\"#CollapsingLeft\" aria-controls=\"CollapsingLeft\" aria-expanded=\"false\" aria-label=\"Toggle navigation\"></button>
            {% endif %}
          {% if container_navbar %}
          </div>
          {% endif %}
        </nav>
      {% endblock %}
    </header>


    <!--  Full width slider partial:index.partial.html -->
    <div class=\"slider-container\">
      <div class=\"slider-control left inactive\"></div>
      <div class=\"slider-control right\"></div>
      <ul class=\"slider-pagi\"></ul>
      <div class=\"slider\">
        <div class=\"slide slide-0 active\">
          <div class=\"slide__bg\"></div>
          <div class=\"slide__content\">
            <svg class=\"slide__overlay\" viewBox=\"0 0 720 405\" preserveAspectRatio=\"xMaxYMax slice\">
              <path class=\"slide__overlay-path\" d=\"M0,0 150,0 500,405 0,405\" />
            </svg>
            <div class=\"slide__text\">
              <h2 class=\"slide__text-heading\">Why insect Farming?</h2>
              <p class=\"slide__text-desc\">Did you know that wild birds and free-range poultry can consume insects inform an adult,
                 larval and pupal naturally? Register now to learn how you can use insects as feed supplement for your livestock.</p>
            <!--  <a class=\"slide__text-link\"></a>-->
            </div>
          </div>
        </div>
        <div class=\"slide slide-1 \">
          <div class=\"slide__bg\"></div>
          <div class=\"slide__content\">
            <svg class=\"slide__overlay\" viewBox=\"0 0 720 405\" preserveAspectRatio=\"xMaxYMax slice\">
              <path class=\"slide__overlay-path\" d=\"M0,0 150,0 500,405 0,405\" />
            </svg>
            <div class=\"slide__text\">
              <h2 class=\"slide__text-heading\">Why Black Soldier Fly?</h2>
              <p class=\"slide__text-desc\">Unlike House Flies, Black Soldier Flies are not known to transmit diseases.
Insect that can turn farm waste into rich animal feed. This providing a valuable alternative to conventional yet expensive protein feed.
</p>

            </div>
          </div>
        </div>
        <div class=\"slide slide-2\">
          <div class=\"slide__bg\"></div>
          <div class=\"slide__content\">
            <svg class=\"slide__overlay\" viewBox=\"0 0 720 405\" preserveAspectRatio=\"xMaxYMax slice\">
              <path class=\"slide__overlay-path\" d=\"M0,0 150,0 500,405 0,405\" />
            </svg>
            <div class=\"slide__text\">
              <h2 class=\"slide__text-heading\">WASTE MANAGEMENT</h2>
              <p class=\"slide__text-desc\">We all generate a lot of waste every day in our kitchens,
                we dispose most of it but for a wise framer will use it to feed insects that will in turn feed his/her poultry, fish and pigs.”</p>

            </div>
          </div>
        </div>
        <div class=\"slide slide-3\">
          <div class=\"slide__bg\"></div>
          <div class=\"slide__content\">
            <svg class=\"slide__overlay\" viewBox=\"0 0 720 405\" preserveAspectRatio=\"xMaxYMax slice\">
              <path class=\"slide__overlay-path\" d=\"M0,0 150,0 500,405 0,405\" />
            </svg>
            <div class=\"slide__text\">
              <h2 class=\"slide__text-heading\">WHY JOIN RESSECT?</h2>
              <p class=\"slide__text-desc\">You will be part of an open exchange about and discussion of the
                practical day-to-day working steps required to operate a productive Black Soldier Fly facility.</p>

            </div>
          </div>
        </div>







      </div>
    </div>
    <!-- partial -->

 <!--
<div class=\"jumbotron jumbotronFront masthead\" id=\"home\">
  <div class=\"container jumbotronText\">
    <h1 style=\"background: black;display: -webkit-inline-box;padding: 5px;\">Ressect4farmers</h1>
    <p style=\"background: black;display: -webkit-inline-box;padding: 5px;\">Sleek, smooth, powerful, and revolutionary jQuery lightbox plugin for creative and ambitious web designers and developers.</p>


    <p>
      <a href=\"https://goo.gl/nl0Bg\" class=\"btn btn-primary btn-large\">Download iLightBox&nbsp;&nbsp;<i class=\"icon-download-alt icon-white\"></i></a>
      <a href=\"https://goo.gl/DlaJq\" class=\"btn btn-info btn-large\">Download for WordPress&nbsp;&nbsp;<i class=\"icon-download-alt icon-white\"></i></a>
      <a href=\"discussion.html\" class=\"btn btn-success btn-large\">Discussion&nbsp;&nbsp;<i class=\"icon-comment icon-white\"></i></a>
    </p>
   -->
   <!--
    <ul class=\"masthead-links\">
      <li>

      </li>
    </ul>
    -->
  </div>
</div>





    {% if page.highlighted %}
      <div class=\"highlighted\">
        <aside class=\"{{ container }} section clearfix\" role=\"complementary\">
          {{ page.highlighted }}
        </aside>
      </div>
    {% endif %}
    {% if page.featured_top %}
      {% block featured %}
        <div class=\"featured-top\">
          <aside class=\"featured-top__inner section {{ container }} clearfix\" role=\"complementary\">
            {{ page.featured_top }}
          </aside>
        </div>
      {% endblock %}
    {% endif %}

















    <!-- partial:index.partial.html -->
    <div class=\"cards\" style=\"background: aliceblue; padding-top: 4em;padding-bottom: 10em;\">


  <!--    <div class=\"introExplanationSec\"> -->
        <h3 style=\"text-align:center;padding-bottom: 5px;color: #000;text-decoration: overline;\">How It Works</h3>
      <!--  <p style=\"text-align:center;color: #000;text-decoration: overline; font-weight: 600;\">Are you a farmer and interested in making extra cash with your kitchen or agricultural waste?
          <ol style=\"text-align:center;color: #000; font-weight: 400;\">
            <li>You want to stop spending money on chemical fertilizer</li>
            <li>You want to lower your risk of being dependent on a single crop harvest</li>
            <li>You want to make an extra income out of it</li>
          </ol>

        </p>
      </div>-->


      <div class=\"card\">
        <img class=\"card__image\" src=\" {{ directory }}/images/how-to/1.jpg\" alt=\"wave\" />
        <div class=\"card-title\" style=\"margin-bottom:0\">
          <a href=\"#\" class=\"toggle-info btn\">
            <span class=\"left\"></span>
            <span class=\"right\"></span>
          </a>
          <h2>
              1. Register with us</br></br>
              <!--<small>Image from unsplash.com</small>-->
          </h2>
        </div>
        <div class=\"card-flap flap1\">
          <div class=\"card-description\">
          Register with us today and get free unlimited access to our knowledge center
          and our forum where you will get open exchange about and discussion of the practical
           day-to-day working steps required to operate a Black Soldier Fly facility form researchers and farmers alike.
          </div>
        <!--  <div class=\"card-flap flap2\">
            <div class=\"card-actions\">
              <a href=\"#\" class=\"btn\">Read more</a>
            </div>
          </div> -->
        </div>
      </div>

        <div class=\"card\">
        <img class=\"card__image\" src=\"{{ directory }}/images/how-to/2.jpg\" alt=\"beach\" />
        <div class=\"card-title\" style=\"margin-bottom:0\">
          <a href=\"#\" class=\"toggle-info btn\">
            <span class=\"left\"></span>
            <span class=\"right\"></span>
          </a>
          <h2>
              2. Learn on knowledge center</br></br>
            <!--  <small>Image from unsplash.com</small> -->
          </h2>
        </div>
        <div class=\"card-flap flap1\">
          <div class=\"card-description\">
            There is no need for sophisticated high-end technology to start a Black Soildier Fly Facility or Skilled Labour.
             Get access to our Knowlegde centre anywhere, anytime and Begin Today.
          </div>
      <!--    <div class=\"card-flap flap2\">
            <div class=\"card-actions\">
              <a href=\"#\" class=\"btn\">Read more</a>
            </div>
          </div> -->
        </div>
      </div>

      <div class=\"card\">
        <img class=\"card__image\" src=\"{{ directory }}/images/how-to/3.jpg\" alt=\"mountain\" />
        <div class=\"card-title\" style=\"margin-bottom:0\">
          <a href=\"#\" class=\"toggle-info btn\">
            <span class=\"left\"></span>
            <span class=\"right\"></span>
          </a>
          <h2>
            3. Order the specialized starter kit
            <!--  <small>Image from unsplash.com</small>-->
          </h2>
        </div>
        <div class=\"card-flap flap1\">
          <div class=\"card-description\">
          Insect farming uses a tiny fraction of the feed, water, land and labor needed to raise traditional
          livestock such as cattle or pigs. Why not start today with a BST STARTER KIT from as low as 0000 Kshs.
          </div>
        <!--  <div class=\"card-flap flap2\">
            <div class=\"card-actions\">
              <a href=\"#\" class=\"btn\">Read more</a>
            </div>
          </div> -->
        </div>
      </div>

      <div class=\"card\">
        <img class=\"card__image\" src=\"{{ directory }}/images/how-to/4.jpg\" alt=\"field\" />
        <div class=\"card-title\" style=\"margin-bottom:0\">
          <a href=\"#\" class=\"toggle-info btn\">
            <span class=\"left\"></span>
            <span class=\"right\"></span>
          </a>
          <h2>
              4. Sell on our digital market place
            <!--  <small>Image from unsplash.com</small> -->
          </h2>
        </div>
        <div class=\"card-flap flap1\">
          <div class=\"card-description\">
            Easy-to-use. Quick go-to-market with Over 500 farmers visit this page every day.
            Choose from thousands of great products to connect buyers and sellers with the all-in one platform.
            Currently, you can buy or sell a kilo of dried larvae at between Sh35 and Sh40.
          </div>
      <!--    <div class=\"card-flap flap2\">
            <div class=\"card-actions\">
              <a href=\"#\" class=\"btn\">Read more</a>
            </div>
          </div> -->
        </div>
      </div>
    </div>
    <!-- partial -->
      <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js'></script>


































<!--
<div class=\"section\" style=\"/* background: #435964c7; */padding-top: 50px;padding-bottom: 50px;background: rgba(0, 0, 0, 0.14);\">
<div class=\"introExplanationSec\">
  <h3 style=\"text-align:center;padding-bottom: 30px;color: #000;text-decoration: overline;\">How It Works</h3>
  <p style=\"text-align:center;color: #000;text-decoration: overline; font-weight: 600;\">Are you a farmer and interested in making extra cash with your kitchen or agricultural waste?
    <ol style=\"text-align:center;color: #000; font-weight: 400;\">
      <li>You want to stop spending money on chemical fertilizer</li>
      <li>You want to lower your risk of being dependent on a single crop harvest</li>
      <li>You want to make an extra income out of it</li>
    </ol>

  </p>
</div>


   <div class=\"container\">
      <div class=\"row align-items-stretch\">
         <div class=\"col-lg-4 order-lg-2\" style=\" padding-bottom: 30px; \">
            <div class=\"scaling-image h-100\">
               <div class=\"frame h-100\">
                  <div class=\"feature-img-bg h-100\" style=\"\">
                  </div>
               </div>
            </div>
         </div>
         <div class=\"col-md-6 col-lg-4 feature-1-wrap d-md-flex flex-md-column order-lg-1\" style=\" padding-bottom: 30px; \">
            <div class=\"feature-1 d-md-flex\">
               <div class=\"align-self-center\">
                  <span class=\"flaticon-chakra display-4 text-primary\"></span>
                  <h5>1. Register Here</h5>
                  <p>Register on this website and receive free access to the Knowledge Center, StarterKit ordering and Digital marketplace.
                  </br></br>
                  Register now and get updates as soon as the platform is finalized
                  </p>

               </div>
            </div>
            <div class=\"feature-1 d-md-flex\">
               <div class=\"align-self-center\">
                  <span class=\"flaticon-lotus display-4 text-primary\"></span>
                  <h5>3. Order the specialized StarterKit</h5>
                  <p>After having easily acquired knowledge
                    you have the possibility to order a dedicated StarterKit
                    allowing you to optimally produce your insects.</p>
               </div>
            </div>
         </div>
         <div class=\"col-md-6 col-lg-4 feature-1-wrap d-md-flex flex-md-column order-lg-3\" style=\" padding-bottom: 30px; \">
            <div class=\"feature-1 d-md-flex\">
               <div class=\"align-self-center\">
                  <span class=\"flaticon-chakra-1 display-4 text-primary\"></span>
                  <h5>2. Learn on Knowledge Center</h5>
                  <p>Get the full knowledge and every information around insect production for free. Enroll on lectures, watch learning videos and read the articles.</p>
               </div>
            </div>
            <div class=\"feature-1 d-md-flex\">
               <div class=\"align-self-center\">
                  <span class=\"flaticon-yoga display-4 text-primary\"></span>
                  <h5>4. Sell on digital marketplace</h5>
                  <p>Take advantage of the easiest way to sell your products via the \"marketplace\" section on the website. On the marketplace you can find local aquaculture, poultry and pig farmers and sell directly via the website.</p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div> -->

<!--Categories-->

<script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>

<section id=\"section-feature\" class=\"row\">
    <div class=\"container\">
        <ul>
            <li id=\"sf-innovation\" class=\"col-md-3 col-sm-6 col-xs-12\">
                <div class=\"sf-wrap\">

                    <div class=\"sf-mdl-left\">

                        <div class=\"sf-icon\">

                            <i class=\"fa fa-fw fa-book fa-5x\"></i>
                        </div>
                        <h3>Ressect Resource</h3>

                    </div>
                    <div class=\"sf-mdl-right\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-book fa-5x\"></i>
                        </div>
                        <h3>Ressect Resource</h3>

                    </div>

                    <div class=\"sf-mdl-left-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-book fa-5x\"></i>
                        </div>

                        <h3><a href=\"#\">Ressect Resource</a>
                        </h3>
                        <p>There is no need for sophisticated high-end technology to start a Black Soildier Fly Facility or Skilled Labour.
                          Get access to our Knowlegde centre anywhere, anytime and Begin Today.</p>

                    </div>
                    <div class=\"sf-mdl-right-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-book fa-5x\"></i>
                        </div>
                        <h3><a href=\"#\">Ressect Resource</a></h3>
                        <p>Curabitur blandit tempus ardua ridiculus sed magna. Integer legentibus erat a ante historiarum dapibus.</p>
                    </div>
                </div>
            </li>

            <li id=\"sf-community\" class=\"col-md-3 col-sm-6 col-xs-12\">
                <div class=\"sf-wrap\">
                    <div class=\"sf-mdl-left\">
                        <div class=\"sf-icon\">

                            <i class=\"fa fa-fw fa-shopping-basket fa-5x\"></i>
                        </div>
                        <h3>Ressect Marketplace</h3>
                    </div>
                    <div class=\"sf-mdl-right\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-shopping-basket fa-5x\"></i>
                        </div>
                        <h3>Ressect Marketplace</h3>
                    </div>

                    <div class=\"sf-mdl-left-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-shopping-basket fa-5x\"></i>
                        </div>
                        <h3><a href=\"#\">Ressect Marketplace</a></h3>
                        <p>Easy-to-use. Quick go-to-market with Over 500 farmers visit this page everyday. Choose from thousands of great products to connect buyers and sellers with the all-in one platform </p>
                    </div>
                    <div class=\"sf-mdl-right-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-shopping-basket fa-5x\"></i>
                        </div>
                        <h3><a href=\"#\">Ressect Marketplace</a></h3>
                        <p>Easy-to-use. Quick go-to-market with Over 500 farmers visit this page everyday. Choose from thousands of great products to connect buyers and sellers with the all-in one platform </p>
                    </div>
                </div>
            </li>

            <li id=\"sf-academy\" class=\"col-md-3 col-sm-6 col-xs-12\">
                <div class=\"sf-wrap\">
                    <div class=\"sf-mdl-left\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-info fa-5x\"></i>
                        </div>
                        <h3>Starter kit</h3>
                    </div>
                    <div class=\"sf-mdl-right\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-info fa-5x\"></i>
                        </div>
                        <h3>Starter kit</h3>
                    </div>

                    <div class=\"sf-mdl-left-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-info fa-5x\"></i>
                        </div>
                        <h3><a href=\"#\">Starter kit</a></h3>
                        <p>Starter Kit has all of the basic necessities required to get you started. It contains Explain the items.</p>
                    </div>
                    <div class=\"sf-mdl-right-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-info fa-5x\"></i>
                        </div>
                        <h3><a href=\"#\">Starter kit</a></h3>
                        <p>Starter Kit has all of the basic necessities required to get you started. It contains Explain the items.</p>
                    </div>
                </div>
            </li>

            <li id=\"sf-academy\" class=\"col-md-3 col-sm-6 col-xs-12\">
                <div class=\"sf-wrap\">
                    <div class=\"sf-mdl-left\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-comments fa-5x\"></i>
                        </div>
                        <h3>Forum</h3>
                    </div>
                    <div class=\"sf-mdl-right\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-comments fa-5x\"></i>
                        </div>
                        <h3>Forum</h3>
                    </div>

                    <div class=\"sf-mdl-left-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-comments fa-5x\"></i>
                        </div>
                        <h3><a href=\"#\">Forum</a></h3>
                        <p>This community is ready to help with answers to your questions and ongoing support.</p>
                    </div>
                    <div class=\"sf-mdl-right-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-comments fa-5x\"></i>
                        </div>
                        <h3><a href=\"#\">Forum</a></h3>
                        <p>This community is ready to help with answers to your questions and ongoing support.</p>
                    </div>
                </div>
            </li>


                <div class=\"sf-wrap\">
                    <div class=\"sf-mdl-left\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-star-half-o fa-5x\"></i>
                        </div>
                        <h3>Source File Included</h3>
                    </div>
                    <div class=\"sf-mdl-right\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-star-half-o fa-5x\"></i>
                        </div>
                        <h3>Source File Included</h3>
                    </div>

                    <div class=\"sf-mdl-left-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-star fa-5x\"></i>
                        </div>
                        <h3><a href=\"#\">Source File Included</a></h3>
                        <p>Morbi fringilla convallis sapien, id pulvinar odio volutpat. Contra legem facit qui id facit quod lex prohibet.</p>
                    </div>
                    <div class=\"sf-mdl-right-full\">
                        <div class=\"sf-icon\">
                            <i class=\"fa fa-fw fa-star fa-5x\"></i>
                        </div>
                        <h3><a href=\"#\">Source File Included</a></h3>
                        <p>Morbi fringilla convallis sapien, id pulvinar odio volutpat. Contra legem facit qui id facit quod lex prohibet.</p>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>




























<!--Parteners-->


<div class=\"gradient-container\">
<h1 style=\"position:absolute;left:44%;color:#fff; padding-top: 10px;\">Partners </h1>
    <div class=\"text row justify-content-md-center\">


  <div class=\"col col-lg-2\" style=\" margin: 20px; \">
  <div class=\"partner-box\">
   <img src=\"themes/bootstrap_barrio/subtheme/images/repic-logo.png\" style=\"max-width: 90px; margin-top: 10px;\"/>
   <span class=\"partner-head\" style=\"font-size: 50px;font-weight: 500;color: #f1a658;\">REPIC


   </span></div></div>
   <div class=\"col-md-auto\" style=\"margin:20px;font-size:1.2em; font-weight:700;\">
          <ul style=\"list-style: none;\">
            <li>Schweizerische Eidgenossenschaft</li>
            <li>Confèdèration suisse</li>
            <li>Confederazione Svizzera</li>
            <li>Confederaziun svizra</li>
          </ul>
</div>


        <div class=\"col-lg-5\" style=\"margin:20px;font-size:1.2em;\">
          <ul style=\"list-style: none;\">
            <li>State Secretariat for Economic Affairs SECO</li>
            <li>Swiss Agency for Development and Cooperation SDC</li>
            <li>Federal Office for the Environment FOEN</li>
            <li>Swiss Federal Office of Energy SFOE</li>
          </ul>
      </div>
  </div>
  <!-- SVG Grid Background Start    -->
  <div class=\"grid-background\">
    <svg width=\"100vw\" height=\"100vh\" xmlns=\"http://www.w3.org/2000/svg\">
    <defs>

      <pattern id=\"grid\" width=\"20\" height=\"20\" patternUnits=\"userSpaceOnUse\">
        <rect width=\"80\" height=\"80\" fill=\"url(#smallGrid)\"/>
        <path d=\"M 80 0 L 0 0 0 80\" fill=\"none\" stroke=\"white\" stroke-width=\"1\"/>
      </pattern>
    </defs>

    <rect width=\"100%\" height=\"100%\" fill=\"url(#grid)\" />
  </svg>
  </div>
  <!--SVG Grid Background End    -->
  <!-- Animation Start    -->
  <div class=\"slide-lines\">
    <!-- HORIZONTAL LINES -->
    <div class=\"line hz-line\"></div>
    <div class=\"line hz-line\"></div>
    <div class=\"line hz-line\"></div>
    <!-- VERTICAL LINES -->
    <div class=\"line vert-line\"></div>
    <div class=\"line vert-line\"></div>
    <div class=\"line vert-line\"></div>
  <!--Animation End  -->
</div>

</div>






  <!--Main section-->
  {#
    <div id=\"main-wrapper\" class=\"layout-main-wrapper clearfix\">
      {% block content %}
        <div id=\"main\" class=\"container\">
          {{ page.breadcrumb }}
          <div class=\"row row-offcanvas row-offcanvas-left clearfix\">
              <main{{ content_attributes }}>
                <section class=\"section col-8\">
                  <a id=\"main-content\" tabindex=\"-1\"></a>
                  {{ page.content }}
                </section>
              </main>
            {% if page.sidebar_first %}
              <div{{ sidebar_first_attributes }}>
                <aside class=\"section\" role=\"complementary\">
                  {{ page.sidebar_first }}
                </aside>
              </div>
            {% endif %}
            {% if page.sidebar_second %}
              <div{{ sidebar_second_attributes }}>
                <aside class=\"section\" role=\"complementary\">
                  {{ page.sidebar_second }}
                </aside>
              </div>
            {% endif %}
          </div>
        </div>
      {% endblock %}
    </div>

#}










          {% if page.featured_bottom_first or page.featured_bottom_second or page.featured_bottom_third %}
      <div class=\"featured-bottom\">
        <aside class=\"{{ container }} clearfix\" role=\"complementary\">
          {{ page.featured_bottom_first }}
          {{ page.featured_bottom_second }}
          {{ page.featured_bottom_third }}
        </aside>
      </div>
    {% endif %}


    <footer class=\"site-footer\" style=\"height: 100%;background-position: center;background-repeat: no-repeat;background-size: cover;\">


    <!--
    <div class=\"container-fluid\" id=\"footerSuga\">
      <div class=\"row\">

          <div class=\"col-md-3 leftSpace\">
            <div><img src=\"themes/bootstrap_barrio/subtheme/images/logo.png\" style=\"max-width: 130px; margin-top: 10px;\"/></div>
          </div>
          <div class=\"col-md-6 leftSpace\" style=\"padding: 20px;\">

            <iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/QGgcoZKympI\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>
          </div>


          <div class=\"col-md-3 leftSpace\">


            <script async src=\"https://static.addtoany.com/menu/page.js\"></script>
            <!-- AddToAny END
            <h5>About Us</h5>
            <h5>About Us</h5>
            <h5>About Us</h5>

            <!-- AddToAny BEGIN
            <div class=\"a2a_kit a2a_kit_size_32 a2a_default_style\" style=\"margin-top:50px;\">
                <a class=\"a2a_button_facebook\"></a>
                <a class=\"a2a_button_twitter\"></a>
                <a class=\"a2a_button_google_plus\"></a>
            </div>

             <h5>Ressect GmbH</br>
                Im Werd 6</br>
                8952 Schlieren</h5>
          </div>

          <div class=\"container\" style=\"max-width: 980px;\">
          <p style=\"font-family: cursive;\">
              Ressec4farmers is a project from a Swiss animal feed company and
              together with Egerton University we are running an agricultural project in Nakuru County.

            Our project aims to teach local farmers the science of insect farming.
            Farmers can use their agricultural wastes to grow insects which they then can in turn feed to
             their livestock animals. We want to offer farmers in Nakuru an online course on how to produce insects.

            Insect farming is not only good for the environment but is also improving the lives of many farmers in the world.
             We believe that this platform will positively impact farmers in Nakuru County.
          </p>
          </div>

    </div>
  </div> -->







      {% block footer %}
        <div class=\"{{ container }}\">
          {% if page.footer_first or page.footer_second or page.footer_third or page.footer_fourth %}
            <div class=\"site-footer__top clearfix\">
              {{ page.footer_first }}
              {{ page.footer_second }}
              {{ page.footer_third }}
              {{ page.footer_fourth }}
            </div>
          {% endif %}
          {% if page.footer_fifth %}
            <div class=\"site-footer__bottom\">
              {{ page.footer_fifth }}
            </div>
          {% endif %}
        </div>
      {% endblock %}




      <div style=\" color: white; text-align: -webkit-center; font-size: 0.8em; padding-top: 20px;\">Ressect4farmers &copy; 2019</div>
    </footer>
  </div>
</div>
", "themes/bootstrap_barrio/subtheme/templates/custom/page--front.html.twig", "/var/www/Ressect4farmers/web/themes/bootstrap_barrio/subtheme/templates/custom/page--front.html.twig");
    }
}
